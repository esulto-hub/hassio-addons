# Hassio addons

## Installation

Adding this add-ons repository to your Home Assistant instance is
pretty easy. Follow [the official instructions](https://home-assistant.io/hassio/installing_third_party_addons) on the
website of Home Assistant, and use the following URL:

```
https://gitlab.com/esulto-hub/hassio-addons
```

## Provided addons

- Activity assistant

- Bitwarden

- Emulated HUE

- Knxd

- NodeJS runner

- Portainer

- Traefik

## Credits

Most of code are imported from theese repositories:
- [https://github.com/alex3305/home-assistant-addons](https://github.com/alex3305/home-assistant-addons)
- [https://github.com/alexbelgium/hassio-addons](https://github.com/alexbelgium/hassio-addons)
- [https://github.com/cogsmith/homeassistant-nodejs](https://github.com/cogsmith/homeassistant-nodejs)
- [https://github.com/da-anda/hass-io-addons](https://github.com/da-anda/hass-io-addons)
- [https://github.com/hass-emulated-hue/hassio-repo](https://github.com/hass-emulated-hue/hassio-repo)
- [https://github.com/tcsvn/activity-assistant](https://github.com/tcsvn/activity-assistant)

